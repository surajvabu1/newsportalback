package util;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by gregmi on 6/2/15.
 */
public class DateUtil {

    public static Calendar getCalendar(Date time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        return calendar;
    }

    public static Date getLastDateOfMonth(Date date) {
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getFirstDateOfMonth(Date date) {
        Calendar cal = getCalendar(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getToday(Date date) {
        Calendar cal = getCalendar(date);
        return cal.getTime();
    }

    public static int getTotalDaysInMonth() {
        Calendar cal = getCalendar(new Date());
        return cal.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static int getTodaysDate() {
        Calendar cal = getCalendar(new Date());
        return cal.get(Calendar.DAY_OF_MONTH);
    }
    public static int getYear() {
        Calendar cal = getCalendar(new Date());
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth() {
        Calendar cal = getCalendar(new Date());
        return cal.get(Calendar.MONTH);
    }

    public static int getDay() {
        Calendar cal = getCalendar(new Date());
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static String displayDay(int day) {
        return day+"-"+months.get(getMonth())+"-"+getYear();
    }

    public static String displayMonth(int month) {
        return months.get(month)+"-"+getYear();
    }


    public static final Map<Integer,String> months = new HashMap<Integer, String>(){{
        put(0,"Jan");
        put(1,"Feb");
        put(2,"Mar");
        put(3,"Apr");
        put(4,"May");
        put(5,"Jun");
        put(6,"Jul");
        put(7,"Aug");
        put(8,"Sep");
        put(9,"Oct");
        put(10,"Nov");
        put(11,"Dec");
    }};

    public static String getTime() {
        SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm:ss");
        TimeZone timeZone1 = TimeZone.getTimeZone("Asia/Katmandu");
        printFormat.setTimeZone(timeZone1);
        return printFormat.format(new Date());
    }
}

