package newsportalback

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile

import newsportalback.CurrentEvents
class CurrentEventsController {

    def saveEvents() {
        if(session.getAttribute("user")!=null){
        User user = (User) session.getAttribute("user")
        if (request.method == 'POST') {
            // create domain object and assign parameters using data binding
            def ce = new CurrentEvents(params)
            if (!ce.save()) {
                // validation failed, render registration page again
                return [currentevents: ce]
            } else {
                // validate/save ok, store user in session, redirect to homepage
                ce.base64Value = params.currentImage.bytes.encodeBase64().toString();
                session.user = user
                redirect(controller: 'main')
            }
        }
        }

    }

    def newsInformation() {
                JSONObject object = new JSONObject()
                JSONArray array = new JSONArray()
                def currentNews = CurrentEvents.findAll()
                for (CurrentEvents cn : currentNews) {
                    JSONObject inner = new JSONObject()
                    inner.put("id", cn.id)
                    inner.put("title", cn.title)
                    Date d = cn.lastUpdated
                    inner.put("description", cn.description)
                    inner.put("lastUpdated", d.toString())
                    inner.put("base64Value", cn.base64Value)
                    array.add(inner)
                }
                object.put("data", array)
                render object


    }
}
