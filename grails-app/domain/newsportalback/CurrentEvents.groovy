package newsportalback

import java.sql.Blob

class CurrentEvents {

    String title
    String description
    String section
    String place
    Date dateCreated
    Date lastUpdated

    byte[] currentImage
    String base64Value

    static constraints = {
        title(blank:false, nullable:false)
        description(blank:false, nullable:false)
        section(blank: false, nullable: false)
//        place(unique: true)
        //maxSize: 200K
        currentImage(maxSize: 204800)
        base64Value(maxSize: 536870912)
    }
}
